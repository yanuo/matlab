function varargout = face_recog_gui(varargin)
% FACE_RECOG_GUI MATLAB code for face_recog_gui.fig
%      FACE_RECOG_GUI, by itself, creates a new FACE_RECOG_GUI or raises the existing
%      singleton*.
%
%      H = FACE_RECOG_GUI returns the handle to a new FACE_RECOG_GUI or the handle to
%      the existing singleton*.
%
%      FACE_RECOG_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FACE_RECOG_GUI.M with the given input arguments.
%
%      FACE_RECOG_GUI('Property','Value',...) creates a new FACE_RECOG_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before face_recog_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to face_recog_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help face_recog_gui

% Last Modified by GUIDE v2.5 18-Jan-2018 22:59:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @face_recog_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @face_recog_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before face_recog_gui is made visible.
function face_recog_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to face_recog_gui (see VARARGIN)

% Choose default command line output for face_recog_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes face_recog_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);
    imshow('blobs.png');
    
    names = [
        "cloney"
        "obama"
        "tacher"
        "damon"
        ];

    celeb      = struct('name', "", 'faces', zeros(60,40,5), 'face', zeros(60,40), 'projection', zeros(1, 20), 'projections', zeros(1, 20, 5), 'error', zeros(1, 5));
    celebs([]) = struct('name', "", 'faces', zeros(60,40,5), 'face', zeros(60,40), 'projection', zeros(1, 20), 'projections', zeros(1, 20, 5), 'error', zeros(1, 5));

    celebs = load_celebs(names, celebs, celeb);
    celebs = compute_face_average(celebs);
    [V,D]  = compute_eigs(celebs);
    celebs = compute_projections(celebs, V);
    
    handles.names  = names;
    handles.celeb  = celeb;
    handles.celebs = celebs;
    handles.V = V;
%     handles.D = D;
    
    guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = face_recog_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    [FileName,PathName] = uigetfile('*.png','Select the image file');
    f = strcat(PathName, FileName)
    set(handles.text1, 'string', FileName);
    img = imread(f);
    imshow(img);
    
    handles.filename = f;
    guidata(hObject, handles);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    set(handles.radiobutton1, 'Value', 1)
%     handles.my_var

    unknown      = struct('name', "", 'face', zeros(60,40), 'projection', zeros(1, 20), 'recover', zeros(60,40), 'error', zeros(1, 5));
    unknowns([]) = struct('name', "", 'face', zeros(60,40), 'projection', zeros(1, 20), 'recover', zeros(60,40), 'error', zeros(1, 5));

    target_name = handles.filename % compare this celebrity against all 
    celebs = handles.celebs;
         v = handles.V;
    celebs = compare_all(target_name, celebs, unknown, v);
    id = looks_like(celebs)
    radios = [
        handles.radiobutton1
        handles.radiobutton2
        handles.radiobutton3
        handles.radiobutton4
        ];
    set(radios(id), 'Value', 1);

    
    
    

% names = [
%     "cloney"
%     "obama"
%     "tacher"
%     "damon"
%     ];
% 
% celeb      = struct('name', "", 'faces', zeros(60,40,5), 'face', zeros(60,40), 'projection', zeros(1, 20), 'projections', zeros(1, 20, 5), 'error', zeros(1, 5));
% celebs([]) = struct('name', "", 'faces', zeros(60,40,5), 'face', zeros(60,40), 'projection', zeros(1, 20), 'projections', zeros(1, 20, 5), 'error', zeros(1, 5));

% celebs = load_celebs(names, celebs, celeb);
% celebs = compute_face_average(celebs);
% [V,D]  = compute_eigs(celebs);
% celebs = compute_projections(celebs, V);

% plot_faces(celebs);
% plot_faces_average(celebs);
% plot_celeb_eigs(V,D);
% plot_projections(celebs);

unknown      = struct('name', "", 'face', zeros(60,40), 'projection', zeros(1, 20), 'recover', zeros(60,40), 'error', zeros(1, 5));
unknowns([]) = struct('name', "", 'face', zeros(60,40), 'projection', zeros(1, 20), 'recover', zeros(60,40), 'error', zeros(1, 5));

% target = 2; % compare this celebrity against all 
% celebs = compare_all(names(target), celebs, unknown, V);
% id = looks_like(celebs)

function [I] = looks_like(celebs)
    matchs = [4];
    for j = 1:4
        matchs(j) = mean(celebs(j).error);
    end
    [M,I] = min(matchs);

% function [unknown] = compare_one(name, target, celebs, unknown, V)
%     unknown.name = name;
%     unknown.face = resize2gray(char(strcat('../img/test/tacher/', unknown.name, '.png')));
%     unknown.projection = face_projection(unknown.face, V);
%     unknown.recover = reshape(V*unknown.projection', 60, 40);
%     for i = 1:5
%         unknown.error(i) = face_error(unknown.projection, celebs(target).projections(:,:,i));
%     end

function [celebs] = compare_all(name, celebs, unknown, V)
    unknown.name = name;
    filename = name;
    unknown.face = resize2gray(filename);
    unknown.projection = face_projection(unknown.face, V);
    unknown.recover = reshape(V*unknown.projection', 60, 40);
    for k = 1:4
        for i = 1:5
            celebs(k).error(i) = face_error(unknown.projection, celebs(k).projections(:,:,i));
        end
    end

function [fig] = plot_projections(celebs)
	fig = figure();
    for j = 1:4
        subplot(2,2,j), bar(celebs(j).projection(2:20)), set(gca, 'Xlim', [0 20], 'Ylim', [-2000 2000], 'Xtick', [], 'Ytick', []), text(12, -1700, char(celebs(j).name) , 'Fontsize', [15])
    end

function [celebs] = compute_projections(celebs, V)
    for j = 1:4
        for i = 1:5
            celebs(j).projections(:,:,i) = face_projection(celebs(j).faces(:,:,i), V);
        end
        celebs(j).projection = face_projection(celebs(j).face, V);
    end

function [celebs] = compute_face_average(celebs)
    for j = 1:4
        celebs(j).face = average_face(celebs(j).faces);
    end

function [fig] = plot_celeb_eigs(V,D)
	fig = figure();
    for j = 1:5
        subplot(2,3,j), facel=reshape(V(:,j), 60, 40); pcolor(flipud(facel)), shading interp, colormap(gray), set(gca, 'Xtick', [], 'Ytick', []);
    end
    subplot(2,3,6), semilogy(diag(D), 'ko', 'Linewidth', [2]);
    set(gca, 'Fontsize', [14])

function [V,D] = compute_eigs(celebs)
    A = zeros(4*5, 60*40);
    for j = 1:4
        for i = 1:5
            A((j-1)*5+i,:) = reshape(celebs(j).faces(:,:,i), 1, 60*40);
        end
    end

    C = (A') * (A); % correlation matrix
    size(C)

    [V,D] = eigs(C, 20, 'lm'); % V -> eigenvectors, D -> diagonal matrix, eigenvalues

function [fig] = plot_faces_average(celebs)
	fig = figure();
    for j = 1:size(celebs, 2)
        subplot(2,2,j),pcolor(flipud(celebs(j).face)), shading interp, colormap(gray), set(gca, 'Xtick', [], 'Ytick', []);
    end

function [fig] = plot_faces(celebs)
	fig = figure();
    for j = 1:size(celebs, 2)
        for i = 1:5
            subplot(4,5,(j-1)*5+i), pcolor(flipud(celebs(j).faces(:,:,i))), shading interp, colormap(gray), set(gca, 'Xtick', [], 'Ytick', []);
        end
    end

function [celebs] = load_celebs(names, celebs, celeb)
    for j = 1:size(names, 1)
        celebs(j) = load_celeb(names(j), celeb);
    end

function [celeb] = load_celeb(name, celeb)
    celeb.name = name;
    for i = 1:5
        celeb.faces(:,:,i) = resize2gray(char(strcat('../img/cropped/', celeb.name, '/', int2str(i), '.png')));
    end

function [image_gray] = resize2gray(image_rgb)
	image_gray = imresize(double(rgb2gray(imread(image_rgb))),[60 40]);

function [average] = average_face(faces)
    f = 0;
    for i = 1 : 5
        f = f + faces(:,:,i);
    end
    average = f/5;

function [projection] = face_projection(face, v)
    projection = reshape(face, 1, 60*40) * v;

function [error] = face_error(projection, projection_new)
	error = norm(projection - projection_new)/norm(projection);
