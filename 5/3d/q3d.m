
clc;
clear all;

means = [];
for i = 1 : 10
    filename = strcat('rand_', num2str(i), '.txt');
    means = [means csvread(filename)];
end

means

means_matrix = sort_decreasing(means, length(means))'

size(means_matrix)

csvwrite('sort.dat', means_matrix);

function [row_matrix] = sort_decreasing(row_matrix, n)
    if n == 1
        return;
    end
    for i = 1 : n-1
        row_matrix = swap_decreasing(i, row_matrix);
    end
    row_matrix = sort_decreasing(row_matrix, n-1);
end

function [row_matrix] = swap_decreasing(i, row_matrix)
    if row_matrix(i) < row_matrix(i+1)
        tmp  = row_matrix(i+1);
        row_matrix(i+1) = row_matrix(i);
        row_matrix(i) = tmp;
    end
end
