
clc;
clear all;

A = [-4 -3 5 13 18 25 28 26 26 14 6 -2];

filename = 'file1.txt';

csvwrite(filename, A);

if ~exist(filename, 'file') == 2
    return;
end

B = csvread(filename);
disp(B);
