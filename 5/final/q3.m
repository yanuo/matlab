
clc;
clear all;

means = [];
for i = 1 : 10
    A = rand(5);
    filename = strcat('rand_', num2str(i));
    save(strcat(filename, '.dat'), char(A));
    csvwrite(strcat(filename, '.txt'), mean1d(A));
    means = [means csvread(strcat(filename, '.txt'))];
end

means_matrix = sort_decreasing(means, length(means))'
size(means_matrix)
save('sort.dat', '-ascii', 'means_matrix');

function [mean] = mean1d(matrix)
    sum = 0;
    for i = 1 : size(matrix, 1)
        for j = 1 : size(matrix, 2)
            sum = sum + matrix(i, j);
        end
    end
    mean = sum / (i*j);
end

function [row_matrix] = sort_decreasing(row_matrix, n)
    if n == 1
        return;
    end
    for i = 1 : n-1
        row_matrix = swap_decreasing(i, row_matrix);
    end
    row_matrix = sort_decreasing(row_matrix, n-1);
end

function [row_matrix] = swap_decreasing(i, row_matrix)
    if row_matrix(i) < row_matrix(i+1)
        tmp  = row_matrix(i+1);
        row_matrix(i+1) = row_matrix(i);
        row_matrix(i) = tmp;
    end
end
