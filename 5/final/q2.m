
clc;
clear all;

A = [-4 -3 5 13 18 25 28 26 26 14 6 -2];

filename = 'Beijing_temp.dat';

save(filename, 'A');

if ~exist(filename, 'file') == 2
    return;
end

load(filename, '-mat')

plot(A)

lowest = sort_decreasing(A, length(A));
lowest = lowest(length(lowest)-10+1:end);
disp(lowest(end))

function [row_matrix] = sort_decreasing(row_matrix, n)
    if n == 1
        return;
    end
    for i = 1 : n-1
        row_matrix = swap_decreasing(i, row_matrix);
    end
    row_matrix = sort_decreasing(row_matrix, n-1);
end

function [row_matrix] = swap_decreasing(i, row_matrix)
    if row_matrix(i) < row_matrix(i+1)
        tmp  = row_matrix(i+1);
        row_matrix(i+1) = row_matrix(i);
        row_matrix(i) = tmp;
    end
end
