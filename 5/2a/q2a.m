
clc;
clear all;

A = [-4 -3 5 13 18 25 28 26 26 14 6 -2];

filename = 'Beijing_temp.dat';

save(filename, 'A');

if ~exist(filename, 'file') == 2
    return;
end

load(filename, '-mat')

plot(A)
