
clc;
clear all;

for i = 1 : 10
    A = rand(5);
    filename = strcat('rand_', num2str(i), '.dat');
    save(filename, 'A');
end