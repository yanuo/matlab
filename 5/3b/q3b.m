
clc;
clear all;

for i = 1 : 10
    A = rand(5);
    save_matrix(i, A);
    save_matrix_mean(i, A);
end

function save_matrix(i, matrix)
    filename = strcat('rand_', num2str(i), '.dat');
    save(filename, char(matrix));
end

function save_matrix_mean(i, matrix)
    mean = mean1d(matrix)
    filename = strcat('rand_', num2str(i), '.txt');
    csvwrite(filename, mean);
end

function [mean] = mean1d(matrix)
    sum = 0;
    for i = 1 : size(matrix, 1)
        for j = 1 : size(matrix, 2)
            sum = sum + matrix(i, j);
        end
    end
    mean = sum / (i*j);
end
