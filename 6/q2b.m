
clc;
clear all;
close all;

% Change step = 0.001 to match homework!
step = 0.05;
x = -1.5 : step : 1.5;
y =   -1 : step : 1;
z = -1.5 : step : 1.5;

[X,Y,Z] = meshgrid(x, y, z);

f = (X.^2 + 9/4 * Y.^2 + Z.^2 - 1).^3 - X.^2 .* Z.^3 - 9/80 * Y.^2 .* Z.^3;

isosurface(X,Y,Z,f,0)

axis equal

axis_handle = gca;
    
for i = 1 : 10
    if mod(i,2)
        k = 2;
    else
        k = .5;
    end
    resize_axis(k, axis_handle);
    pause(.5);
end

function resize_axis(k, axis_handle)
    axis_handle.XLim = axis_handle.XLim * k;
    axis_handle.YLim = axis_handle.YLim * k;
    axis_handle.ZLim = axis_handle.ZLim * k;
end
