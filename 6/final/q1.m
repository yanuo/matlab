
clc;
clear all;
close all;

% a)
img = imread('pout.tif');

% b)
img_histeq = histeq(img);

% c)
figure;

subplot(2,2,1);
imshow(img);

img_histogram = subplot(2,2,2);
histogram(img_histogram, img);

subplot(2,2,3);
imshow(img_histeq);

img_histeq_histogram = subplot(2,2,4);
histogram(img_histeq_histogram, img_histeq);
