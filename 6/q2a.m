
clc;
clear all;
close all;

step = 0.05;
x = -1.5 : step : 1.5;
y =   -1 : step : 1;
z = -1.5 : step : 1.5;

[X,Y,Z] = meshgrid(x, y, z);

f = (X.^2 + 9/4 .* Y.^2 + Z.^2 - 1).^3 - X.^2 .* Z.^3 - 9/80 .* Y.^2 .* Z.^3;

isosurface(X,Y,Z,f,0)

    axis equal
colormap flag

