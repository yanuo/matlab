clc
clear all

x = [];
y = [];
for i = -180 : 180
    x = [x i];
    y = [y sin(i*pi/180)];
end
bneg = y < 0;      
hold on

plot(x(bneg), y(bneg), 'red');
plot(x(~bneg), y(~bneg), 'blue');