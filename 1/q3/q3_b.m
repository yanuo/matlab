clc
clear

x = [];
y = [];
z = [];
for i = -180 : 180
    x = [x i];
    y = [y sin(i*pi/180)];
    z = [z cos(i*pi/180)];
end
