clc
clear

x = [];
y = [];
z = [];
for i = -180 : 180
    x = [x i];
    y = [y sin(i*pi/180)];
    z = [z cos(i*pi/180)];
end
yneg = y < 0;     
zneg = z < 0;      
hold on

plot(x(~yneg), y(~yneg), 'blue');
plot(x(yneg), y(yneg), 'redo');

plot(x(~zneg), z(~zneg), 'cyan');
plot(x(zneg), z(zneg), 'cyano');

% plot(x(yneg), z(zneg), 'redo');
% plot(x, z, 'cyano')
