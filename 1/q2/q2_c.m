clc
clear all

for row = 1 : 100
    for col = 1 : 200
        A(row,col) = rand();
    end
end

n = 0
for row = 1 : 100
    for col = 1 : 200
        if A(row,col) > .2 && A(row,col) < .8
            n = n + 1;
        end
    end
end
disp(n)