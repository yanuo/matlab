clc
clear all

for row = 1 : 100
    for col = 1 : 200
        A(row,col) = rand();
    end
end

for row = 1 : 100
    for col = 1 : 200
        if A(row,col) < .2 || A(row,col) > .8
            A(row,col) = nan;
        end
    end
end