clc
clear all

x = [-2    -1    0   1    2  ]';
y = [-12.8 -8.8 -6.3 -5.8 5.2]';
B = table(x,y)

E = [ones(size(x)) x x.^2];

c = E\y
