clc
clear all

a = 2

b = [-1 3;
      6 0]

c = [0 1;
     2 0]

d = [-2.2 -0.1;
      1.9  1.2;
      2.1  0.1]

e = ceil(d)