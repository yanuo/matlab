
Try to solve the system of linear equations.
What happens? 

Matlab reports:

Warning: Matrix is close to singular or badly
scaled. Results may be inaccurate. RCOND =
7.680788e-18.

Can you see why? 

det(A) = 7.3275e-15 is too much close to ZERO.
When the determinant of a square matrix A is zero, A is not invertible. When it does have an inverse, its possible to find a unique solution, e.g., to the equation Ax=B given some vector B.

Again check your answer using matrix multiplication. 
Is the answer “correct”?

No.