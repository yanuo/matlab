
narcissistics = [];

for n = 100 : 1000
    s = split(string(n),'')';
    s = s(2:end-1);
    k = length(s);

    sum = 0;
    for j = 1 : k
        sum = sum + str2double(s(j))^k;
    end

    if n == sum
        narcissistics = [narcissistics sum];
    end
end 

for i = 1 : length(narcissistics)
    fprintf('%d %d \n', i, narcissistics(i));

end