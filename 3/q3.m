
clc
clear all

f = @(x) x - cos(x);
fsolve(f, 0)
tol = 1e-4;

a = 0;
b = pi/2;

fa = a - cos(a);
fb = b - cos(b);

while abs(a-b) > tol
    c = (a+b)/2;
    fc = c - cos(c);
    if fa * fc < 0
        b = c;
    else
        a = c;
    end
end
c