
% clc
% clear all

A = input('Enter matrix A: ');
B = input('Enter matrix B: ');

 disp(A);
 disp(B);

try
    C = A * B;
    disp(C);
catch ME
      msg = ['Dimension mismatch occurred: First argument has ', ...
            num2str(size(A,2)),' columns while second has ', ...
            num2str(size(B,1)),' rows.'];
        causeException = MException('MATLAB:myCode:dimensions',msg);
        ME = addCause(ME,causeException);
   rethrow(ME)
end 
