
clc;
clear all;

leapyears = leap_years(1952, 2017);

fprintf('\n');

for i = 1 : length(leapyears)
    fprintf('%d %d\n', i, leapyears(i));
end

fprintf('\nThere are %d leap years.\n', length(leapyears));

function [years] = leap_years(from_year, to_year)
    years = [];
    for year = from_year : to_year
        if isleap(year)
            years = [years year];
        end
    end
end

function [leap] = isleap(year)
    leap = step_a(year);
end

function [leap] = step_a(year)
    if ~mod(year, 4)
        leap = step_b(year);
    else
        leap = step_e(year);
    end
end

function [leap] = step_b(year)
    if ~mod(year, 100)
        leap = step_c(year);
    else
        leap = step_d(year);
    end
end

function [leap] = step_c(year)
    if ~mod(year, 400)
        leap = step_d(year);
    else
        leap = step_e(year);
    end
end

function [leap] = step_d(year)
    leap = true;
    fprintf('%d is leap year.\n', year);
end

function [leap] = step_e(year)
    leap = false;
    fprintf('%d\n', year);
end
