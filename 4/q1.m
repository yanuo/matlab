
clc;
clear all;

A = magic(4)

mean1d(A)

function [mean] = mean1d(matrix)
    sum = 0;
    for i = 1 : size(matrix, 1)
        for j = 1 : size(matrix, 2)
            sum = sum + matrix(i, j);
        end
    end
    mean = sum / (i*j);
end
