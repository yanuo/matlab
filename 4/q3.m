
clc;
clear all;

A = ceil(randn(10,1)*100)'

sort_decreasing(A, length(A))

function [row_matrix] = sort_decreasing(row_matrix, n)
    if n == 1
        return;
    end
    for i = 1 : n-1
        row_matrix = swap_decreasing(i, row_matrix);
    end
    row_matrix = sort_decreasing(row_matrix, n-1);
end

function [row_matrix] = swap_decreasing(i, row_matrix)
    if row_matrix(i) < row_matrix(i+1)
        tmp  = row_matrix(i+1);
        row_matrix(i+1) = row_matrix(i);
        row_matrix(i) = tmp;
    end
end
